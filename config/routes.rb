Rails.application.routes.draw do
  


  root "users#index"
  get  "top/profile"
  resources  :users   
  resources  :skills, :except => [:show]
  get "users/:userID" => "users#show"
  get "users/skills/like/:id/:userID" =>"skills#like"

  get 'skills/index'

  get 'skills/show'

  get 'skills/new'

  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
