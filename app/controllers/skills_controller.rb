class SkillsController < ApplicationController
  def index
  end

  def show
  end

  def new
  end
  def create
    @skill=Skill.new(params.require(:skill).permit(:name,:good,:userID))  
    @skill.save
    redirect_to :back
  end

  def like
   @skill=Skill.find(params[:id])
   @skill.good+=1
   @skill.save           
   redirect_to :back
  end

end
