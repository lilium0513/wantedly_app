class UsersController < ApplicationController
  def index
    @users = User.all
  end

  def show
   @user=User.find(params[:id])
   @skills=Skill.where(userID: params[:id]) 
  
  end

  def new
  end

   def create
     @user=User.new(params.require(:user).permit(:name))
     @user.save
     redirect_to :back
   end
end
